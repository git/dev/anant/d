# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-lang/dmd-bin/dmd-bin-1.014.ebuild,v 1.1 2007/05/20 21:23:08 anant Exp $

inherit eutils

MY_P=${P/-bin/}
MY_P=${MY_P/-/.}

DESCRIPTION="Digital Mars D Compiler"
HOMEPAGE="http://www.digitalmars.com/d/"
SRC_URI="http://ftp.digitalmars.com/${MY_P}.zip"

LICENSE="DMD"
RESTRICT="mirror nostrip"
SLOT="0"
KEYWORDS="~x86"

IUSE="phobos tango"

LOC="/opt/dmd"
S="${WORKDIR}"

DEPEND="sys-apps/findutils"
RDEPEND="amd64? ( app-emulation/emul-linux-x86-compat )
	x86? ( >=virtual/libstdc++-3.3 )"
PDEPEND="phobos? ( dev-libs/phobos )
	tango? ( dev-libs/tango )"

pkg_setup() {
	if use phobos && use tango; then
		eerror "                                                 "
		eerror "You have selected both phobos and tango as USE   "
		eerror "flags! Only a single standard library may be     "
		eerror "chosen.                                          "
		eerror "                                                 "
		die "Multiple libraries selected"
	fi

	if ! use phobos && ! use tango; then
		eerror "                                                 "
		eerror "You have selected neither phobos or tango in     "
		eerror "your USE flags! Exactly one standard library     "
		eerror "must be chosen.                                  "
		eerror "                                                 "
		die "No library selected"
	fi
}

src_unpack() {
	unpack ${A}

	# Remove unneccessary files
	rm -r ${S}/dmd/src/phobos
	rm -r ${S}/dmd/lib
	rm -r ${S}/dm
	rm ${S}/dmd/bin/*.dll ${S}/dmd/bin/*.exe ${S}/dmd/bin/readme.txt
	rm ${S}/dmd/bin/sc.ini ${S}/dmd/bin/windbg.hlp

	# Cleanup line endings
	cd ${S}/dmd
	edos2unix `find . -name '*.c' -type f`
	edos2unix `find . -name '*.d' -type f`
	edos2unix `find . -name '*.ddoc' -type f`
	edos2unix `find . -name '*.h' -type f`
	edos2unix `find . -name '*.mak' -type f`
	edos2unix `find . -name '*.txt' -type f`
	edos2unix `find samples -name '*.html' -type f`

	# Fix permissions
	fperms guo=r `find . -type f`
	fperms guo=rx `find . -type d`
	fperms guo=rx bin/dmd bin/dumpobj bin/obj2asm bin/rdmd
}

src_install() {
	cd ${S}/dmd

	# Broken dmd.conf
	# http://d.puremagic.com/issues/show_bug.cgi?id=278
	mv bin/dmd bin/dmd.bin
	
	# Man pages
	doman man/man1/dmd.1
	doman man/man1/dumpobj.1
	doman man/man1/obj2asm.1
	rm -r man

	# Install
	mkdir "${D}/opt"
	mv "${S}/dmd" "${D}/opt/dmd"

	# Set PATH
	doenvd "${FILESDIR}/25dmd"
}

pkg_postinst () {
	ewarn "The DMD Configuration file has been disabled,    "
	ewarn "and will be re-enabled when:                     "
	ewarn "                                                 "
	ewarn "http://d.puremagic.com/issues/show_bug.cgi?id=278"
	ewarn "                                                 "
	ewarn "has been fixed. Meanwhile, please supply all your"
	ewarn "configuration options in the /opt/dmd/bin/dmd    "
	ewarn "shell script, *after* a standard library:        "
	ewarn "phobos or tango has been successfully merged.    "
	ewarn "                                                 "
	ewarn "You may need to run:                             "
	ewarn "                                                 "
	ewarn "env-update && source /etc/profile                "
	ewarn "                                                 "
	ewarn "to be able to use the compiler immediately.      "
	ewarn "                                                 "
}
