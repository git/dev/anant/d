# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils toolchain-funcs

DESCRIPTION="Platform library for the D programming language"
HOMEPAGE="http://www.dsource.org/projects/tango/"
SRC_URI="http://downloads.dsource.org/projects/${PN}/${PV}/${P}-src.tar.gz"

LICENSE="|| ( AFL-2.1 BSD )"
SLOT="0"
KEYWORDS="~x86"

# Tango with GDC needs to be cleaned up
#IUSE="gdc"
IUSE=""

DEPEND="!dev-libs/phobos"
RDEPEND=""
S=${WORKDIR}/${P}-src

# Helper functions for compiling
tango_filter_file() {
	FILE=$1
	if [ "`echo $FILE | grep win32`" -o "`echo $FILE | grep Win32`" ]; then
		return 1
	fi
	if [ "`echo $FILE | grep darwin`" ]; then
		return 1
	fi
	return 0
}

tango_compile_file() {
	DC=$1
	FILENAME=$3
	OBJNAME=`echo $FILENAME | sed -e 's/\.d//' | sed -e 's/\//\./g'`
	OBJNAME=${OBJNAME}.o

	if tango_filter_file $OBJNAME; then
		$DC -c -inline -release -O -version=Posix -version=Tango -of$OBJNAME $FILENAME
		ar -r lib/$2 $OBJNAME
		rm $OBJNAME
	fi
}

tango_compile_library() {
	cd ${S}
	for fil in `find tango -name '*.d'`
	do
		tango_compile_file $1 $2 $fil		
	done
	ranlib lib/$2
}

tango_compile_dmd() {
	for f in "dmd-posix.mak common/tango/posix.mak compiler/dmd/posix.mak"
	do
		fperms guo=rw $f
		sed -i -e "s:^DC=.*:DC=/opt/dmd/bin/dmd.bin:" $f
		edos2unix $f
	done

	OLDHOME=$HOME
	export HOME=${S}/lib
	make clean -fdmd-posix.mak
	make lib doc install -fdmd-posix.mak || die "Compile failed!"
	make clean -fdmd-posix.mak
	export HOME=$OLDHOME

	tango_compile_library /opt/dmd/bin/dmd.bin libtango.a
}

tango_compile_gdc() {
	GDC_VER="`gdc --version | grep 'gdc' | sed 's/^.*gdc \([0-9]*\.[0-9]*\).*$/\1/'`"
	GDC_MAJOR="`echo $GDC_VER | sed 's/\..*//'`"
	GDC_MINOR="`echo $GDC_VER | sed 's/.*\.//'`"

	if [ "$GDC_MAJOR" = "0" -a "$GDC_MINOR" -lt "23" ]; then
		eerror "                                                 "
		eerror "This version of Tango requires GDC 0.23 or newer!"
		eerror "                                                 "
		die "Required version of GDC not found"
	fi

	HOST_ARCH="`./compiler/gdc/config.guess | sed 's/-.*//'`"
	ADD_CFLAGS=
	if [ "$HOST_ARCH" = "powerpc" -a ! "`./compiler/gdc/config.guess | grep darwin`" ]; then
		ADD_CFLAGS="-mregnames"
	fi

	pushd ./compiler/gdc
	./configure || die "Configure failed!"
	popd

	OLDHOME=$HOME
	export HOME=${S}/lib
	make clean -fgdc-posix.mak 
	make lib doc install -fgdc-posix.mak ADD_CFLAGS="$ADD_CFLAGS" || die "Compile failed!"
	make clean -fgdc-posix.mak
	export HOME=$OLDHOME

	tango_compile_library gdmd libgtango.a
}

# Helper functions for installing
tango_install_dmd() {
	LOC=/opt/dmd
	cd ${S}
	insinto ${LOC}/lib
	doins ${S}/lib/*.a
	insinto ${LOC}/include/d/tango
	doins ${S}/object.di
	mv ${S}/tango ${D}${LOC}/include/d/tango
	mv ${S}/std ${D}${LOC}/include/d/tango

	cat <<END > "dmd"
#!/bin/sh
${LOC}/bin/dmd.bin -I${LOC}/include/d/tango -L${LOC}/lib/libtango.a -version=Tango -version=Posix \$*
END
	fperms guo=rx dmd
	mkdir ${D}${LOC}/bin
	cp dmd ${D}${LOC}/bin
}

tango_install_gdc() {
	insinto /usr/$(get_libdir)/gcc/$CHOST/$(gcc-fullversion)
	doins ${S}/lib/*.a
	insinto /usr/include/d/$(gcc-fullversion)
	doins ${S}/object.di
	doins ${S}/tango/*
}

pkg_setup() {
	if use gdc; then
		if ! built_with_use sys-devel/gcc d; then
			eerror "                                                 "
			eerror "You have enabled the gdc USE flag, but gcc was   "
			eerror "not compiled with the d USE flag. Either merge   "
			eerror "sys-devel/gcc with d enabled, or                 "
			eerror "dev-lang/dmd-bin with the tango USE flag.        "
			eerror "                                                 "
			die "No suitable D compiler found!"
		else
			if built_with_use dev-lang/dmd-bin tango; then
				export ENABLE_LIBRARY="both"
				einfo "                                                 "
				einfo "dev-lang/dmd-bin with tango USE flag detected,   "
				einfo "the Tango standard library with be built and     "
				einfo "enabled for the DMD compiler.                    "
				einfo "                                                 "
				einfo "You have also used the gdc use flag, and         "
				einfo "Tango will be built and enabled for the GDC      "
				einfo "compiler.                                        "
				einfo "                                                 "
			else
				export ENABLE_LIBRARY="gdc"
				einfo "                                                 "
				einfo "The Tango standard library will be built and     "
				einfo "enabled for the GDC compiler.                    "
				einfo "                                                 "
			fi
		fi
	else
		if ! built_with_use dev-lang/dmd-bin tango; then
			eerror "                                                 "
			eerror "dev-lang/dmd-bin was not built with the tango    "
			eerror "USE flag. Please enable it if you wish to use    "
			eerror "Tango with DMD. If you want to use Tango with    "
			eerror "GDC, please enable the gdc USE flag for this     "
			eerror "package.                                         "
			eerror "                                                 "
			die "No suitable D compiler found!"
		else
			export ENABLE_LIBRARY="dmd"
			einfo "                                                 "
			einfo "the Tango standard library with be built and     "
			einfo "enabled for the DMD compiler.                    "
			einfo "                                                 "
		fi
	fi
}

src_compile() {
	cd ${S}/lib

	case "$ENABLE_LIBRARY" in
		
		"dmd" )
		tango_compile_dmd
		;;

		"gdc" )
		tango_compile_gdc
		;;

		"both" )
		tango_compile_dmd
		tango_compile_gdc
		;;
	
	esac
}

src_install() {
	case "$ENABLE_LIBRARY" in
		
		"dmd" )
		tango_install_dmd
		;;

		"gdc" )
		tango_install_gdc
		;;

		"both" )
		tango_install_dmd
		tango_install_gdc
		;;
	
	esac
}

pkg_postinst() {
	einfo "                                                   "
	einfo "*Please* run env-update && source /etc/profile     "
	einfo "before using your D compilers!                     "
	einfo "                                                   "
}
