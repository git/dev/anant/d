# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="The Phobos standard library for DMD"
HOMEPAGE="http://www.digitalmars.com/d/"
SRC_URI="http://ftp.digitalmars.com/dmd.${PV}.zip"

LICENSE="DMD"
RESTRICT="mirror nostrip"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

LOC="/opt/dmd"
S="${WORKDIR}"

DEPEND=">=dev-lang/dmd-bin-1.014"
RDEPEND=""

src_unpack() {
	unpack ${A}

	# Remove unneccesary file
	mv ${S}/dmd/src/phobos ${S}/
	rm -r ${S}/dmd ${S}/dm
	mkdir -p ${S}/dmd/lib ${S}/dmd/src ${S}/dmd/bin
	mv ${S}/phobos ${S}/dmd/src/
}

src_compile() {
	cd ${S}/dmd/src/phobos
	sed -i -e "s:DMD=.*:DMD=${LOC}/bin/dmd.bin:" linux.mak internal/gc/linux.mak
	edos2unix linux.mak internal/gc/linux.mak
	make -flinux.mak
	cp libphobos.a ${S}/dmd/lib/

	# Clean up
	make -flinux.mak clean
	rm internal/gc/*.o

	cd ${S}/dmd/bin
	cat <<END > "dmd"
#!/bin/sh
${LOC}/bin/dmd.bin -I${LOC}/src/phobos -L${LOC}/lib/libphobos.a \$*
END
}

src_install() {
	cd ${S}
	fperms guo=rx dmd/bin/dmd
	mkdir "${D}/opt"
	mv "${S}/dmd" "${D}/opt/dmd"
}

pkg_postinst() {
	einfo "                                                   "
	einfo "The Phobos standard library has been built and     "
	einfo "enabled for your DMD compiler. Please modify       "
	einfo "/opt/dmd/bin/dmd as per your compiler requirements "
	einfo "(as a replacement for dmd.conf)                    "
	einfo "                                                   "
	ewarn "*Please* run env-update and source /etc/profile    "
	ewarn "before using the D compiler!                       "
	ewarn "                                                   "
}
